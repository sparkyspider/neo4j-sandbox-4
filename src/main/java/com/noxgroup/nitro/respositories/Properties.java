package com.noxgroup.nitro.respositories;

import com.noxgroup.nitro.domain.Person;
import com.noxgroup.nitro.domain.Property;
import org.springframework.data.neo4j.repository.GraphRepository;

public interface Properties extends GraphRepository<Property> {

    Person findByName(String name);

}
