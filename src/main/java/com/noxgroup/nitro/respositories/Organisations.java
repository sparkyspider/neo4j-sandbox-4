package com.noxgroup.nitro.respositories;

import com.noxgroup.nitro.domain.Organisation;
import org.springframework.data.neo4j.repository.GraphRepository;

public interface Organisations extends GraphRepository<Organisation> {

    Organisation findByName(String name);

}
