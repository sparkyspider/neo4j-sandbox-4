package com.noxgroup.nitro.respositories;

import com.noxgroup.nitro.domain.Person;
import org.springframework.data.neo4j.repository.GraphRepository;

public interface People extends GraphRepository<Person> {

    Person findByName(String name);

}
