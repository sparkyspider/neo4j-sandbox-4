package com.noxgroup.nitro;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class NitroApp {

    public NitroApp() {
        // Empty Constructor
    }

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(NitroApp.class);
    }


}
