package com.noxgroup.nitro.bean;

public enum Right {
    LOGIN, ADMINISTRATE, SEND_QUOTES;
}
