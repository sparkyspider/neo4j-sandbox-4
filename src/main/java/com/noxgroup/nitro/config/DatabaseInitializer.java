package com.noxgroup.nitro.config;

import com.noxgroup.nitro.bean.Right;
import com.noxgroup.nitro.domain.Organisation;
import com.noxgroup.nitro.domain.Person;
import com.noxgroup.nitro.domain.Property;
import com.noxgroup.nitro.respositories.Organisations;
import com.noxgroup.nitro.respositories.People;
import com.noxgroup.nitro.respositories.Properties;
import org.neo4j.ogm.session.Session;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class DatabaseInitializer implements InitializingBean {

    /*** BeforeSave ***/
    /*** JSONIgnore ***/
    /*** RightsEnum (Is there an easier way) Not Working ***/

    @Autowired
    Session session;

    @Autowired
    People people;

    @Autowired
    Organisations organisations;

    @Autowired
    Properties properties;

    @Transactional
    void clearDatabase() {
        session.execute("MATCH (n) OPTIONAL MATCH (n)-[r]-() DELETE n,r");
    }

    @Transactional
    void createNodesAndRelationships() {

        Person mark = new Person("Mark");
        mark.password = "mark123";
        people.save(mark);

        Person richard = new Person("Richard");
        richard.password = "richard123";
        people.save(richard);

        Person charlotte = new Person("Charlotte");
        charlotte.password = "charlotte123";
        people.save(charlotte);

        Person joe = new Person ("Joe");
        people.save(joe);

        Organisation tourInfo = new Organisation("tourInfo");
        organisations.save(tourInfo);

        Organisation noxRentals = new Organisation("Nox Rentals");
        noxRentals.addUser(mark, "Administrator", Right.ADMINISTRATE);
        noxRentals.addUser(richard, "Administrator", Right.ADMINISTRATE);
        noxRentals.addContact(mark, "CTO");
        noxRentals.addContact(richard, "Owner");
        organisations.save(noxRentals);

        Organisation classicVillas = new Organisation("Classic Villas");
        classicVillas.addUser(mark, "Administrator", Right.ADMINISTRATE);
        classicVillas.addUser(richard, "Administrator", Right.ADMINISTRATE);
        classicVillas.addUser(charlotte, "Reservations Consultant", Right.LOGIN, Right.SEND_QUOTES);
        classicVillas.addContact(charlotte, "Manager");
        organisations.save(classicVillas);

        Property campsBayTerrace = new Property("Camps Bay Terrace");
        campsBayTerrace.setOwner(tourInfo);
        campsBayTerrace.setManager(noxRentals);
        campsBayTerrace.addAgent(classicVillas);
        properties.save(campsBayTerrace);

        Property seaViews = new Property("Sea Views");
        seaViews.addAgent(classicVillas);
        properties.save(seaViews);

        Property joesPlace = new Property("Joe's Place");
        joesPlace.addAgent(classicVillas);
        joesPlace.setOwner(joe);
        joesPlace.setManager(joe);
        properties.save(joesPlace);

    }

    @Transactional
    public void checkData() {

    }

    void run() {
        clearDatabase();
        createNodesAndRelationships();
        checkData();
    }

    @Transactional

    @Override
    public void afterPropertiesSet() throws Exception {
        run();
    }

}
