package com.noxgroup.nitro.config;

import com.noxgroup.nitro.domain.NitroNode;
import org.neo4j.ogm.session.SessionFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.neo4j.config.Neo4jConfiguration;
import org.springframework.data.neo4j.event.BeforeSaveEvent;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.data.neo4j.server.Neo4jServer;
import org.springframework.data.neo4j.server.RemoteServer;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableNeo4jRepositories(basePackages = "com.noxgroup.nitro")
@EnableTransactionManagement
public class NitroNeo4jConfiguration extends Neo4jConfiguration {

    @Bean
    public Neo4jServer neo4jServer () {
        System.setProperty("username", "neo4j");
        System.setProperty("password", "n0xgr0up4dm1n");
        return new RemoteServer("http://localhost:7474");
    }

    @Bean
    public SessionFactory getSessionFactory() {
        return new SessionFactory("com.noxgroup.nitro.domain");
    }

    @Bean
    ApplicationListener<BeforeSaveEvent> beforeSaveEventApplicationListener() {
        return new ApplicationListener<BeforeSaveEvent>() {
            @Override
            public void onApplicationEvent(BeforeSaveEvent event) {
                System.out.println("Listening to event");
                Object entity = event.getEntity();
                if (entity instanceof NitroNode) {
                     ((NitroNode)entity).beforeSave();
                } else {
                    System.out.println("Not picking it up");
                }
            }
        };
    }

}

