package com.noxgroup.nitro.domain;

import com.noxgroup.nitro.bean.Right;
import org.neo4j.ogm.annotation.*;

import java.util.HashSet;
import java.util.Set;

@NodeEntity
public class Organisation extends NitroNode implements Entity {

    public Organisation() {
        // Empty Constructor
    }

    public Organisation (String name) {
        this.name = name;
    }

    @Relationship(type = "IsUser", direction = Relationship.INCOMING)
    Set<IsUser> users = new HashSet<>();

    @Relationship(type = "HasContact", direction = Relationship.OUTGOING)
    Set<HasContact> contacts = new HashSet<>();

    @org.neo4j.ogm.annotation.Property
    Entity owner;

    public IsUser addUser(Person person, String role) {
        IsUser user = new IsUser(person, this, role);
        this.users.add(user);
        person.users.add(user);
        return user;
    }

    public IsUser addUser(Person person, String role, Right...rights) {
        IsUser isUser = addUser(person, role);
        isUser.addRights(rights);
        return isUser;
    }

    public HasContact addContact(Person person, String role) {
        HasContact contact = new HasContact(this, person, role);
        this.contacts.add(contact);
        return contact;
    }

    public void setOwner (Person person) {
        this.owner = person;
    }

    public void setOwner (Organisation organisation) {
        this.owner = organisation;
    }

    public Entity getOwner() {
        return this.owner;
    }

    @Override
    public void beforeSave() {
        System.out.println("Saving Organisation");
    }
}
