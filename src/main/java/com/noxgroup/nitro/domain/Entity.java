package com.noxgroup.nitro.domain;

public interface Entity {

    String getName();
    void setName(String name);

}
