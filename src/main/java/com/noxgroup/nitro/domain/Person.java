package com.noxgroup.nitro.domain;

import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Property;
import org.neo4j.ogm.annotation.Relationship;

import java.util.HashSet;
import java.util.Set;

@NodeEntity
public class Person extends NitroNode implements Entity {

    public Person() {
        // Empty Constructor
    }

    public Person(String name) {
        this.name = name;
    }

    @Property
    public String password;

    @Relationship(type = "IsUser", direction = Relationship.OUTGOING)
    Set<IsUser> users = new HashSet<>();

    public Set<IsUser> getUserAccounts() {
        return this.users;
    }

    @Override
    public void beforeSave() {
        System.out.println("Saving Person");
    }


}
