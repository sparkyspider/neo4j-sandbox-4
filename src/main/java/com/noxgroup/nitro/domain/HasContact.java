package com.noxgroup.nitro.domain;

import org.neo4j.ogm.annotation.*;
import org.neo4j.ogm.annotation.Property;

@RelationshipEntity(type="HasContact")
public class HasContact {

    @GraphId
    Long id;

    @StartNode
    Organisation organisation;

    @EndNode
    Person person;

    @Property
    String role;

    public HasContact(Organisation organisation, Person person, String role) {
        this.organisation = organisation;
        this.person = person;
        this.role = role;
    }

}
