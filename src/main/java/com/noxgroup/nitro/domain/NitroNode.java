package com.noxgroup.nitro.domain;

import org.neo4j.ogm.annotation.GraphId;

public abstract class NitroNode {

    @GraphId
    Long id;

    public String name;

    public abstract void beforeSave();

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

}
