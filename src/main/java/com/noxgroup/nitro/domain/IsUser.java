package com.noxgroup.nitro.domain;

import com.noxgroup.nitro.bean.Right;
import com.noxgroup.nitro.converters.RightConverter;
import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.Property;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;
import org.neo4j.ogm.annotation.typeconversion.Convert;

import java.util.HashSet;
import java.util.Set;

@RelationshipEntity (type = "IsUser")
public class IsUser {

    @GraphId
    Long id;

    @StartNode
    public Person person;

    @EndNode
    public Organisation organisation;

    @Property
    public String role;

    @Property
    @Convert(RightConverter.class)
    public Set<Right> rights = new HashSet<>();

    public IsUser (Person person, Organisation organisation, String role) {
        this.person = person;
        this.organisation = organisation;
        this.role = role;
    }

    public void addRights(Right...rights) {
        for (Right right : rights) {
            this.rights.add(right);
        }
    }

}
