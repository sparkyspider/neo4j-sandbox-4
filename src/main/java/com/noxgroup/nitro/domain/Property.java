package com.noxgroup.nitro.domain;

import org.neo4j.ogm.annotation.Relationship;

import java.util.HashSet;
import java.util.Set;

public class Property extends NitroNode {

    public Property() {
        // Empty Constructor
    }

    @Relationship(type = "IsOwner", direction = Relationship.INCOMING)
    Entity owner;

    @Relationship(type = "IsManager", direction = Relationship.INCOMING)
    Entity manager;

    @Relationship(type = "IsAgent", direction = Relationship.INCOMING)
    Set<Entity> agents = new HashSet<>();

    public Property(String name) {
        this.name = name;
    }

    @Override
    public void beforeSave() {
        System.out.println("Saving Property");
    }

    public void setOwner(Entity owner) {
        this.owner = owner;
    }

    public Entity getOwner() {
        return owner;
    }

    public void setManager(Entity manager) {
        this.manager = manager;
    }

    public Entity getManager() {
        return this.manager;
    }

    public void addAgent(Entity agent) {
        this.agents.add(agent);
    }

}
