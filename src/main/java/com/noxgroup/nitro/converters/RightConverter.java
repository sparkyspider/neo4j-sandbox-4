package com.noxgroup.nitro.converters;

import com.noxgroup.nitro.bean.Right;
import org.neo4j.ogm.typeconversion.AttributeConverter;

import java.util.HashSet;
import java.util.Set;

public class RightConverter implements AttributeConverter<Set<Right>, String[]> {
    public String[] toGraphProperty(Set<Right>  rights) {
        String[] result = new String[rights.size()];
        int i = 0;
        for (Right right : rights) {
            result[i++] = right.name();
        }
        return result;
    }

    public Set<Right> toEntityAttribute(String[] rights) {
        Set<Right> result = new HashSet<>();
        for (String right : rights) {
            result.add(Right.valueOf(right));
        }
        return result;
    }
}